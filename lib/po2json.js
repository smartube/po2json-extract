const { resolve, extname, basename, dirname, join, sep } = require("path");
const { readdir, stat, readFile, writeFile, mkdir } = require("fs").promises;
const { existsSync, mkdirSync } = require("fs");
const po2json = require("po2json");
const { EventEmitter } = require("events");

const ignoreFolders = ['node_modules'];

class Po2Json extends EventEmitter {
  constructor(options = {}) {
    super();
    const defaultOptions = {
      from: null,
      to: null,
      pretty: false,
      recurcive: false,
      asFolderName: false
    };
    this.options = Object.assign(defaultOptions, options);
  }

  run() {
    this.getLocales()
      .then((list) => {
        this.emit("files", list);
        list.forEach(async (file) => {
          this.emit("start", file);
          return this.translateFile(file);
        });
      })
      .finally(() => {
        this.emit("done");
      });
  }

  translateFile(file) {
    let json = {};
    readFile(file).then((buffer) => {
      let jsonData = po2json.parse(buffer);
      for (let key in jsonData) {
        // Special headers handling, we do not need everything
        if ("" === key) {
          json[""] = {
            language: jsonData[""]["language"],
            "plural-forms": jsonData[""]["plural-forms"],
          };
          continue;
        }
        json[key] =
          jsonData[key].length === 2 && jsonData[key][0] === null
            ? jsonData[key][1] || key // fallback
            : Array.prototype.concat(
              key,
              jsonData[key].map(msg => msg || jsonData[key][0] || key));
      }

      let saveTo = file.replace(".po", ".json");

      if (this.options.to) {
        let filename;
        if (this.options.asFolderName) {
          filename = `${dirname(saveTo).split(sep).slice(-1)[0]}.json`
        } else {
          filename = basename(saveTo);
        }
        if (!existsSync(this.options.to)) {
          mkdirSync(this.options.to, {recursive: true});
        }
        saveTo = join(this.options.to, filename);
      }
      
      writeFile(
        saveTo,
        JSON.stringify(json, null, this.options.pretty ? 4 : 0)
      ).then((err) => {
        if (err) {
          this.emit("error", err);
        } else {
          this.emit("end", resolve(saveTo));
        }
      });
    });
  }

  async getLocales() {
    if (this.options.from.endsWith(".po")) {
      if (existsSync(this.options.from)) {
        return [this.options.from];
      } else {
        this.emit('error', `file not exist ${this.options.from}`);
        return [];
      }
    } else {
      const files = await this.getFiles(this.options.from);
      return files
    }
  }

  async getFiles(dir) {
    const subdirs = await readdir(dir)  
    const files = await Promise.all(
      subdirs.map(async (subdir) => {
        const res = resolve(dir, subdir);
        if (this.options.recurcive && (await stat(res)).isDirectory() && !ignoreFolders.includes(basename(res))) {
          return this.getFiles(res);
        } else {
          return res
        }
      })
    );
    return files.flat().filter(f => extname(f) == ".po");;
  }
}

module.exports = Po2Json;
