# Convert po files to json format

## Install:
add po2json-extract to your project from this repository:
```
  yarn add 'https://bitbucket.org/smartube/po2json-extract/'
```

## Usage:

```
  yarn po2json-extract
```

### options:

--from 
folder to convert files from

--to 
filter to convert complete json files. 
By default it save to the same dir as .po file

--pretty 
pretty format to json

-r, --recurcive
Recurcively walk to subfolder and convert all .po files

--as-folder-name
Save json file as current file folder name: 
`yarn po2json-extract --from ./locales --to ./result -r --as-folder-name # ./locales/ru/app.po -> ./result/ru.json`

Example:

```
yarn po2json-extract --from ./locales -r 
```

